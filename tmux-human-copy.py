#!/usr/bin/python3

import os
import sys
import tty
import time
import tempfile
import subprocess
import termios

COMMON_COMBINATIONS = ['j', 'f', 'k', 'd', 'l', 'a', 'h', 'g', 'jj', 'ff', 'kk', 'dd', 'll', 'aa', 'hh', 'gg']

def launch_in_tmux(w_active, contents_file):
    subprocess.check_output(['tmux', 'split-window', '-h', '-t', w_active, '/home/xand/bin/tmux-human-copy.py run ' + contents_file], universal_newlines=True)

def get_active_window():
    output = None
    o = subprocess.check_output(['tmux', 'list-windows'], universal_newlines=True)
    lines = o.split('\n')
    
    for line in lines:
        if line.find('(active)') != -1:
            output = line.split(':')[0]
            break
        
    return output

def capture_pane_content(w_active):
    pane_contents_file = None

    p_contents = subprocess.check_output(['tmux', 'capture-pane', '-b9', '-pS', '-32768'], universal_newlines=True)
    
    with tempfile.NamedTemporaryFile(delete=False) as temp_file:
        pane_contents_file = temp_file.name
        temp_file.write(p_contents.encode('utf-8'))

    return pane_contents_file

def read_input():
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)

    try:
        tty.setraw(sys.stdin.fileno())
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    
    return ch

def run(c_file):
    contents = []
    
    with open(c_file, mode='r', encoding='utf-8') as f_contents:
        ac = f_contents.readlines()
        for line in ac:
            l = line.strip()
            if len(l) > 0:
                contents += [l]

    # enumerate last 50 lines
    
    h_lines = dict()

    label = 0
    range_start = len(contents) - 20
    if range_start < 0:
        range_start = 0
    for c in range(range_start, len(contents)):
        content = contents[c]

        l = ''
        if label >= len(COMMON_COMBINATIONS):
            l = str(label - len(COMMON_COMBINATIONS) + 1)
        else:
            l = COMMON_COMBINATIONS[label]
            
        h_lines[l] = content
        label += 1

    # print label and content
    for key in list(h_lines.keys()):
        s_key = str(key)
        while len(s_key) < 2:
            s_key = ' ' + key
            
        print(s_key + ': ' + h_lines.get(key))

    reading = True
    u_buffer = ''
    while reading:
        ch = read_input()

        if ch == "y":
            reading = False
        else:
            u_buffer += str(ch)

    s_content = h_lines.get(u_buffer)
    c_temp_file = None
    if s_content:
        p = subprocess.Popen(['xsel', '-b', '-i'],
                             stdin=subprocess.PIPE, shell=False)
        p.stdin.write(s_content.encode('utf-8'))
        p.stdin.close()

    # cleanup
    os.remove(c_file)
    #os.remove(c_temp_file)

if __name__=='__main__':
    mode = None
    if len(sys.argv) > 1:
        mode = sys.argv[1]

    w_active = get_active_window()
    
    if not mode:
        # prepare tmux
        c_file = capture_pane_content(w_active)
        launch_in_tmux(w_active, c_file)
    elif mode == 'run':
        # run
        c_file = sys.argv[2]
        run(c_file)

